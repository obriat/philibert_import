Module Prestashop de création de commandes Philibert
====================================================


Le `Import Command Service` est utilisé pour importer une commande non virtuelle. Ce service crée une
commande pour un expéditeur et l’intègre dans le système informatique de Philibert, qui se
chargera du reste du cycle de vie de la commande.

# TODO
- Créer un transporteur Philibert.
- N'exécuter la création de commande que si ce transporteur est sélectionné.


# Documentation Import Command Service

Reçue par mail le *30/08/2018*.

## Sommaire

- Sommaire
- Introduction
   - Implémentation du service
   - A propos de Import Command Service
   - À propos des tables de définition d'éléments de données
- Import Command Request
   - /commande
      - POST
   - /commande/test
      - POST
- Import Command Request Schema
   - Commande
   - Adresse
   - CommandeLigne
- Import Command Response Schema
   - Response
- Import Command Error Response Schema
   - Response
- Utiliser le service
- Exemple


## Introduction

Ce document décrit l'interface JSON pour le Import Command Service de Philibert.

Le document spécifie les fonctions offertes par le service, les objets JSON utilisés en
paramètre des ressources du service et les objets JSON en réponse du service.

Dans ce document, vous trouverez les tableaux décrivant les objets JSON à fournir pour
chaque ressource.

### Implémentation du service
Ce service est implémenté en utilisant le format JSON. Le client / partenaire est responsable
de l'envoi d'un message JSON au format affiché dans ce document. Le client / partenaire est
responsable de la mise en œuvre de la possibilité de recevoir des messages JSON au
format affiché dans ce document.

### A propos de `Import Command Service`
Le Import Command Service est utilisé pour importer une commande. Ce service crée une
commande pour un expéditeur et l’intègre dans le système informatique de Philibert, qui se
chargera du reste du cycle de vie de la commande.

La composante commune des requêtes est : <https://backoffice.meeple-logistics.com/expediteur>

La description du service est disponible : <https://backoffice.meeple-logistics.com/expediteur?description>

Les outils permettant de tester le service sont disponibles : <https://backoffice.meeple-logistics.com/expediteur?test>

### À propos des tables de définition d'éléments de données

Les tableaux ci-dessous décrivent les objets du Import Command Service :

* Nom : Le nom de l’élément dans l’objet JSON
* Type : Le type ou le format de l’élément
    * Chaîne : Chaîne de caractères
    * Entier : Entier
    * Numérique : Réel. La partie entière et décimale sont séparées par un **“.”**
    * Booléen : Booléen
* Requis :
    * R : Requis
    * O : Optionnel
    * C : Conditionnel
* Description : Une courte description de l’élément


## Import Command Request

### /commande
#### POST

Cette ressource permet d’envoyer un objet Command (cf 3.A) en
JSON et de l’importer dans le système informatique de Philibert sous
l’état “à expédier”.

### /commande/test
#### POST

Cette ressource permet de tester l’envoi d’un objet Command (cf 3.A)
en JSON et de l’importer dans le système informatique de Philibert
sous l’état “annulée”.

## Import Command Request Schema

Voici la liste des tableaux décrivant les objets utilisés par les ressources 
du `Import Command Service`. Des tableaux décrivant les différents éléments de données sont disponible ci-dessous.

### Commande

| Nom                    | Type                     | Requis | Définition                       |
|------------------------|--------------------------|--------|----------------------------------|
| cmd_ref                | Chaîne                   | R      | Référence de la commande         |
| cmd_nom                | Chaîne                   | R      | Nom du client                    |
| cmd_prenom             | Chaîne                   | R      | Prénom du client                 |
| cmd_email              | Chaîne                   | R      | Mail du client                   |
| cmd_adresseFacturation | Adresse                  | R      | Adresse de facturation           |
| cmd_adresseLivraison   | Adresse                  | R      | Adresse de livraison             |
| cmd_transporteurCode   | Chaîne                   | R      | Code transporteur de la commande |
| tabClg                 | Tableau de CommandeLigne | R      | Contenu de la commande           |
 
*Pour la liste des correspondances transporteurs/codes, consulter dans le document “Import
Command Services - Annexes”, la feuille “Transporteurs”.*


### Adresse

| Nom                      | Type   |Requis | Définition                                              |
|--------------------------|--------|-------|---------------------------------------------------------|
| adr_societe              | Chaîne | O     | Raison sociale du destinataire                          |
| adr_civilite             | Chaîne | O     | Civilité du destinataire                                |
| adr_nom                  | Chaîne | R     | Nom du destinataire                                     |
| adr_prenom               | Chaîne | R     | Prénom du destinataire                                  |
| adr_adresse1             | Chaîne | R     | Ligne n°1 de l’adresse du destinataire                  |
| adr_adresse2             | Chaîne | O     | Ligne n°2 de l’adresse du destinataire                  |
| adr_adresse3             | Chaîne | O     | Ligne n°3 de l’adresse du destinataire                  |
| adr_adresse4             | Chaîne | O     | Ligne n°4 de l’adresse du destinataire                  |
| adr_codePostal           | Chaîne | R     | Code postal de l’adresse du destinataire                |
| adr_ville                | Chaîne | R     | Ville de l’adresse du destinataire                      |
| adr_codePays             | Chaîne | R     | Code ISO2 du pays de l’adresse du destinataire          |
| adr_pays                 | Chaîne | O     | Nom du pays de l’adresse du destinataire                |
| adr_codeEtat             | Chaîne | O     | Code de l’état de l’adresse du destinataire             |
| adr_etat                 | Chaîne | O     | Nom de l’état de l’adresse du destinataire              |
| adr_telephone            | Chaîne | O     | Numéro de téléphone de l’adresse du destinataire        |
| adr_telMobile            | Chaîne | O     | Numéro de téléphone mobile de l’adresse du destinataire |
| adr_instructionLivraison | Chaîne | O     | Instruction de livraison pour le transporteur           |
| adr_codePorte1           | Chaîne | O     | Code de la porte n°1 de l’adresse du destinataire       |
| adr_codePorte2           | Chaîne | O     | Code de la porte n°2 de l’adresse du destinataire       |
| adr_interphone           | Chaîne | O     | Code de l’interphone de l’adresse du destinataire       |
| adr_etage                | Chaîne | O     | Numéro de l’étage de l’adresse du destinataire          |
| adr_batiment             | Chaîne | O     | Numéro du bâtiment de l’adresse du destinataire         |


### CommandeLigne

| Nom              | Type      | Requis | Définition                                   |
|------------------|-----------|--------|----------------------------------------------|
| clg_refArticle   | Chaîne    | O      | Référence de l’article                       |
| clg_codeBarre    | Chaîne    | O      | Code barre de l’article                      |
| clg_libArticle   | Chaîne    | R      | Libellé de l’article                         |
| clg_editeur      | Chaîne    | O      | Éditeur de l’article                         |
| clg_qteArticle   | Entier    | R      | Quantité de l’article dans la commande       |
| clg_tx_tva       | Numérique | R      | Taux TVA de l’article (entre 00.00 et 99.99) |
| clg_pu_ttc       | Numérique | R      | Prix unitaire TTC de l’article (en €)        |
| clg_poidsArticle | Chaîne    | O      | Poids de l’article (en kg)                   |

## Import Command Response Schema

### Response

**Nom Type Définition**
succes Booléen Etat du succès de la requête
code Chaîne Code du succès de la requête
description Chaîne Description du succès de la requête


## Import Command Error Response Schema

### Response

**Nom Type Définition**
succes Booléen Etat du succès de la requête
code Chaîne Code d’erreur de la requête
description Chaîne Description de l’erreur de la requête

*Pour la liste des erreurs, consulter dans le document “Import Command Services -
Annexes”, la feuille “Liste des erreurs”.*

## Utiliser le service


Pour pouvoir utiliser le service, vous devrez renseigner dans l’entête HTTP de la requête le
champ “api-key”, avec la valeur que nous vous aurons fourni.

Une clé de test est disponible à la demande.

## Exemple


```json
{
  "cmd_ref": "TEST",
  "cmd_email": "email@test.com",
  "cmd_nom": "Nom",
  "cmd_prenom": "Prénom",
  "cmd_transporteurCode" : "SOCOLISSIMO",
  "cmd_adresseLivraison": {
    "adr_societe": "Philibert",
    "adr_civilite": "Mr.",
    "adr_nom": "Bert",
    "adr_prenom": "Phili",
    "adr_adresse1": "Adresse1",
    "adr_adresse2": "Adresse2",
    "adr_adresse3": "Adresse3",
    "adr_adresse4": "Adresse4",
    "adr_codePostal": "67100",
    "adr_ville": "Strasbourg",
    "adr_codePays": "FR",
    "adr_pays": "France",
    "adr_codeEtat": "CE",
    "adr_etat": "Etat",
    "adr_telephone": "0123456789",
    "adr_telMobile": "0987654321",
    "adr_instructionLivraison": "Instructions",
    "adr_codePorte1": "1",
    "adr_codePorte2": "2",
    "adr_interphone": "3",
    "adr_etage": "4",
    "adr_batiment": "A5"
    },
  "cmd_adresseFacturation": {
    "adr_societe": "Philibert",
    "adr_civilite": "Mr.",
    "adr_nom": "Bert",
    "adr_prenom": "Phili",
    "adr_adresse1": "Adresse1",
    "adr_adresse2": "Adresse2",
    "adr_adresse3": "Adresse3",
    "adr_adresse4": "Adresse4",
    "adr_codePostal": "67100",
    "adr_ville": "Strasbourg",
    "adr_codePays": "FR",
    "adr_pays": "France",
    "adr_codeEtat": "CE",
    "adr_etat": "Etat",
    "adr_telephone": "0123456789",
    "adr_telMobile": "0987654321",
    "adr_instructionLivraison": "Instructions",
    "adr_codePorte1": "1",
    "adr_codePorte2": "2",
    "adr_interphone": "3",
    "adr_etage": "4",
    "adr_batiment": "A5"
  },
  "tabClg" : [
    {
      "clg_refArticle": "Référence_1",
      "clg_codeBarre": "CodeBarre_1",
      "clg_libArticle": "Libellé_1",
      "clg_editeur": "Editeur_1",
      "clg_qteArticle": 1,
      "clg_tx_tva": 20,
      "clg_pu_ttc": 12.34,
      "clg_poidsArticle": 1
    },
    {
      "clg_refArticle": "Référence_2",
      "clg_codeBarre": "CodeBarre_2",
      "clg_libArticle": "Libellé_2",
      "clg_editeur": "Editeur_2",
      "clg_qteArticle": 5,
      "clg_tx_tva": 5.5,
      "clg_pu_ttc": 56.78,
      "clg_poidsArticle": 4
    }
  ]
}
```


