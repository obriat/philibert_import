<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License
 *   (AFL 3.0) International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
  exit;
}

class Philibert_import extends Module {

  protected $config_form = FALSE;

  public function __construct() {
    $this->name = 'philibert_import';
    $this->tab = 'administration';
    $this->version = '0.1.0';
    $this->author = 'Olivier Briat';
    $this->need_instance = 0;

    /**
     * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
     */
    $this->bootstrap = TRUE;

    parent::__construct();

    $this->displayName = $this->l('Import Command Service de Philibert');
    $this->description = $this->l('Le Import Command Service est utilisé pour importer une commande. Ce service crée une
commande pour un expéditeur et l’intègre dans le système informatique de Philibert, qui se
chargera du reste du cycle de vie de la commande.');

    $this->ps_versions_compliancy = ['min' => '1.7', 'max' => _PS_VERSION_];
  }

  /**
   * Don't forget to create update methods if needed:
   * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
   */
  public function install() {
    Configuration::updateValue('PHILIBERT_IMPORT_TEST_MODE', TRUE);

    return parent::install() &&
      $this->registerHook('header') &&
      $this->registerHook('backOfficeHeader') &&
      $this->registerHook('actionOrderStatusUpdate');
  }

  public function uninstall() {
    Configuration::deleteByName('PHILIBERT_IMPORT_TEST_MODE');

    return parent::uninstall();
  }

  /**
   * Load the configuration form
   */
  public function getContent() {
    /**
     * If values have been submitted in the form, process.
     */
    if (((bool) Tools::isSubmit('submitPhilibert_importModule')) == TRUE) {
      $this->postProcess();
    }

    $this->context->smarty->assign('module_dir', $this->_path);

    $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

    return $output . $this->renderForm();
  }

  /**
   * Create the form that will be displayed in the configuration of your module.
   */
  protected function renderForm() {
    $helper = new HelperForm();

    $helper->show_toolbar = FALSE;
    $helper->table = $this->table;
    $helper->module = $this;
    $helper->default_form_language = $this->context->language->id;
    $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

    $helper->identifier = $this->identifier;
    $helper->submit_action = 'submitPhilibert_importModule';
    $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', FALSE)
      . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');

    $helper->tpl_vars = [
      'fields_value' => $this->getConfigFormValues(),
      /* Add values for your inputs */
      'languages' => $this->context->controller->getLanguages(),
      'id_language' => $this->context->language->id,
    ];

    return $helper->generateForm([$this->getConfigForm()]);
  }

  /**
   * Create the structure of your form.
   */
  protected function getConfigForm() {
    return [
      'form' => [
        'legend' => [
          'title' => $this->l('Settings'),
          'icon' => 'icon-cogs',
        ],
        'input' => [
          [
            'type' => 'text',
            'desc' => $this->l('Base URL (without final slash)') . ' (ex: https://backoffice.meeple-logistics.com/expediteur)',
            'name' => 'PHILIBERT_IMPORT_BASE_URL',
            'label' => $this->l('Service base url'),
          ],
          [
            'type' => 'text',
            'col' => 3,
            'prefix' => '<i class="icon icon-envelope"></i>',
            'name' => 'PHILIBERT_IMPORT_API_KEY',
            'label' => $this->l('API key'),
            'desc' => $this->l('Test API key:') . ' <i>C9D6E656ABB44B7DB8DCAFE79C7AAB3F71498375</i>',
          ],
          [
            'type' => 'switch',
            'label' => $this->l('Test mode'),
            'name' => 'PHILIBERT_IMPORT_TEST_MODE',
            'is_bool' => TRUE,
            'desc' => $this->l('Use this module in live mode (not test mode'),
            'values' => [
              [
                'id' => 'test_on',
                'value' => TRUE,
                'label' => $this->l('Enabled'),
              ],
              [
                'id' => 'test_off',
                'value' => FALSE,
                'label' => $this->l('Disabled'),
              ],
            ],
          ],
        ],
        'submit' => [
          'title' => $this->l('Save'),
        ],
      ],
    ];
  }

  /**
   * Set values for the inputs.
   */
  protected function getConfigFormValues() {
    return [
      'PHILIBERT_IMPORT_TEST_MODE' => Configuration::get('PHILIBERT_IMPORT_TEST_MODE', TRUE),
      'PHILIBERT_IMPORT_BASE_URL' => Configuration::get('PHILIBERT_IMPORT_BASE_URL', 'https://backoffice.meeple-logistics.com/expediteur'),
      'PHILIBERT_IMPORT_API_KEY' => Configuration::get('PHILIBERT_IMPORT_API_KEY', 'C9D6E656ABB44B7DB8DCAFE79C7AAB3F71498375'),
    ];
  }

  /**
   * Save form data.
   */
  protected function postProcess() {
    $form_values = $this->getConfigFormValues();

    foreach (array_keys($form_values) as $key) {
      Configuration::updateValue($key, Tools::getValue($key));
    }
  }


  public function hookActionOrderStatusUpdate($params) {

    // états $order_stats = OrderState::getOrderStates($params['id_order']);
    /*
     * Passer la commande à en cours d'expédition
     * Envoyer un mail à Philippe
     *
     */

    $messages = [];

    if (in_array($params['newOrderStatus']->id, [
      Configuration::get('PS_OS_PAYMENT'),
      Configuration::get('PS_OS_WS_PAYMENT'),
    ])) {


      // Get orders details.
      $order_id = (int) $params['id_order'];
      $order = new Order($order_id);
      $address_delivery = new Address($order->id_address_delivery);
      $country_delivery = new Country($address_delivery->id_country);
      $state_delivery = new State ($address_delivery->id_state);
      $address_invoice = new Address($order->id_address_invoice);
      $country_invoice = new Country($address_invoice->id_country);
      $state_invoice = new State ($address_invoice->id_state);

      $customer = new Customer($order->id_customer);

      $id_lang = Context::getContext()->language->id;

      $gender = new Gender($customer->id_gender, $id_lang);

      $articles = $order->getOrderDetailList();

      // Initialise WS call parameters.
      $test_mode = Configuration::get('PHILIBERT_IMPORT_TEST_MODE', TRUE);
      $ws_base_url = Configuration::get('PHILIBERT_IMPORT_BASE_URL', 'https://backoffice.meeple-logistics.com/expediteur');
      $ws_api_key = Configuration::get('PHILIBERT_IMPORT_API_KEY', 'C9D6E656ABB44B7DB8DCAFE79C7AAB3F71498375');
      $action = '/commande';

      $url = $ws_base_url . $action;
      if ($test_mode) {
        $url .= '/test';
      }

      //Initiate cURL.
      $ch = curl_init($url);

      //The JSON data.
      $jsonData = [
        "cmd_ref" => $order->reference,
        "cmd_email" => $customer->email,
        "cmd_nom" => $customer->lastname,
        "cmd_prenom" => $customer->firstname,
        "cmd_transporteurCode" => "SOCOLISSIMO",
        "cmd_adresseLivraison" => [
          "adr_societe" => $address_delivery->company,
          //"adr_civilite" => $address_delivery->,
          "adr_nom" => $address_delivery->lastname,
          "adr_prenom" => $address_delivery->firstname,
          "adr_adresse1" => $address_delivery->address1,
          "adr_adresse2" => $address_delivery->address2,
          //"adr_adresse3" => $address_delivery->,
          //"adr_adresse4" => $address_delivery->,
          "adr_codePostal" => $address_delivery->postcode,
          "adr_ville" => $address_delivery->city,
          "adr_codePays" => $country_delivery->iso_code,
          "adr_pays" => $address_delivery->country,
          "adr_codeEtat" => $state_delivery->iso_code,
          "adr_etat" => $state_delivery->name,
          "adr_telephone" => $address_delivery->phone,
          "adr_telMobile" => $address_delivery->phone_mobile,
          "adr_instructionLivraison" => $address_delivery->other,
          //"adr_codePorte1" => $address_delivery->,
          //"adr_codePorte2" => $address_delivery->,
          //"adr_interphone" => $address_delivery->,
          //"adr_etage" => $address_delivery->,
          //"adr_batiment" => $address_delivery->,
        ],
        "cmd_adresseFacturation" => [
          "adr_societe" => $address_invoice->company,
          //"adr_civilite" => $address_invoice->,
          "adr_nom" => $address_invoice->lastname,
          "adr_prenom" => $address_invoice->firstname,
          "adr_adresse1" => $address_invoice->address1,
          "adr_adresse2" => $address_invoice->address2,
          //"adr_adresse3" => $address_invoice->,
          //"adr_adresse4" => $address_invoice->,
          "adr_codePostal" => $address_invoice->postcode,
          "adr_ville" => $address_invoice->city,
          "adr_codePays" => $country_invoice->iso_code,
          "adr_pays" => $address_invoice->country,
          "adr_codeEtat" => $state_invoice->iso_code,
          "adr_etat" => $state_invoice->name,
          "adr_telephone" => $address_invoice->phone,
          "adr_telMobile" => $address_invoice->phone_mobile,
          "adr_instructionLivraison" => $address_invoice->other,
          //"adr_codePorte1" => $address_invoice->,
          //"adr_codePorte2" => $address_invoice->,
          //"adr_interphone" => $address_invoice->,
          //"adr_etage" => $address_invoice->,
          //"adr_batiment" => $address_invoice->,
        ],
        "tabClg" => [],
      ];

      foreach ($articles as $article) {
        // Ignore virtual products.
        $prod = new Product($article['product_id']);
        if ($prod->getType() !== Product::PTYPE_VIRTUAL) {
          $jsonData['tabClg'][] =
            [
              "clg_refArticle" => $prod->id,
              //"clg_codeBarre" => "CodeBarre_1",
              "clg_libArticle" => $article['product_name'],
              //"clg_editeur" => "Editeur_1",
              "clg_qteArticle" => $article['product_quantity'],
              "clg_tx_tva" => $article['tax_rate'],
              "clg_pu_ttc" => $article['unit_price_tax_incl'],
              "clg_poidsArticle" => $article['product_weight'],
            ];
        }
      }


      //Encode the array into JSON.
      $jsonDataEncoded = json_encode($jsonData);

      //Tell cURL that we want to send a POST request.
      curl_setopt($ch, CURLOPT_POST, 1);

      //Attach our encoded JSON string to the POST fields.
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

      //Set the content type to application/json
      $headers = [
        'Content-Type: application/json',
        'api-key: ' . $ws_api_key,
      ];
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      //return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = FALSE;
      //Execute the request only if bascket is not empty.
      if (!empty($jsonData['tabClg'])) {
        $result = curl_exec($ch);

        if ($result) {
          $response = json_decode($result);
          if ($response->succes) {
            $messages[] = [
              'severity' => 1,
              'txt' => sprintf(
                'La commande %s (%s) a été prise en compte par Philibert.',
                $order->reference,
                $order_id
              ),
              'log' => sprintf(
                'Philibert : commande %s (%s) envoyé à Philibert. %s (%s)',
                $order->reference,
                $order_id,
                $response->description,
                $response->code
              ),
            ];

          }
          elseif ($response->fault) {
            $messages[] = [
              'severity' => 3,
              'txt' => sprintf(
                'Philibert : commande %s (%s) en erreur merci nous contacter.',
                $order->reference,
                $order_id
              ),
              'log' => sprintf(
                'Philibert : commande %s (%s) en erreur chez Philibert. %s %s (%s)',
                $order->reference,
                $order_id,
                $response->fault,
                $response->faultstring,
                $response->detail
              ),
            ];

          }
          else {
            $messages[] = [
              'severity' => 3,
              'txt' => sprintf(
                'Philibert : commande %s (%s) en erreur merci nous contacter.',
                $order->reference,
                $order_id
              ),
              'log' => sprintf(
                'Philibert : commande %s (%s) en erreur chez Philibert. %s (%s)',
                $order->reference,
                $order_id,
                $response->description,
                $response->code
              ),
            ];
          }
        }
        else {
          $messages[] = [
            'severity' => 4,
            'txt' => sprintf(
              'Philibert : commande %s (%s) en erreur merci nous contacter.',
              $order->reference,
              $order_id
            ),
            'log' => sprintf(
              'Philibert : commande %s (%s), impossible de contacter Philibert, code %s',
              $result,
              $order->reference,
              $order_id
            ),
          ];
        }
      }
      else {
        $messages[] = [
          'severity' => 1,
          'txt' => sprintf(
            'Philibert : commande %s (%s) en erreur merci nous contacter.',
            $order->reference,
            $order_id
          ),
          'log' => sprintf(
            'Philibert : commande %s (%s) entièrement virtuelle, rien n\'a été envoyé à Philibert.',
            $order->reference,
            $order_id
          ),
        ];
      }
      //actionPaymentConfirmation


      /*
       *       Mail::Send(
                      (int)$order->id_lang,
                      'download_product',
                      Context::getContext()->getTranslator()->trans(
                          'The virtual product that you bought is available for download',
                          array(),
                          'Emails.Subject',
                          $orderLanguage->locale
                      ),
                      $data,
                      $customer->email,
                      $customer->firstname.' '.$customer->lastname,
                      null,
                      null,
                      null,
                      null,
                      _PS_MAIL_DIR_,
                      false,
                      (int)$order->id_shop
                  );
       */
      //$current_context = Context::getContext();


      $displayMessage = 'displayWarning';
      $message_to_display = 'txt';
      if ($this->context->controller->controller_type !== 'front') {
        $displayMessage = 'adminDisplayWarning';
        $message_to_display = 'log';
      }

      foreach ($messages as $m) {
        $this->$displayMessage($m[$message_to_display]);
        PrestaShopLogger::addLog(
          $m['log'],
          $m['severity'],
          NULL,
          'Philibert_import'
        );
      }
    }
  }
}
